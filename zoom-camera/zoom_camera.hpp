#pragma once

#include <thread>
#include <array>
#include <vector>
#include <string>

#include <opencv2/opencv.hpp>

namespace thi_sensor_fusion {

class ZoomCamera {
public:

  static const uint8_t I2C_CHIP_ADDRESS = 0x0C;
  static const uint8_t I2C_ZOOM_REGISTER_ADDRESS = 0x00;
  static const uint8_t I2C_ZOOM_RESET_ADDRESS = 0x00 + 0x0A;
  static const uint8_t I2C_FOCUS_REGISTER_ADDRESS = 0x01;
  static const uint8_t I2C_FOCUS_RESET_ADDRESS = 0x01 + 0x0A;
  static const uint8_t I2C_IRCUT_REGISTER_ADDRESS = 0x0C;

  struct CameraInfo {
    uint8_t usb_index;
    std::array<uint8_t, 16>	serial_number;
  };

  enum class I2cMode {
    mode_8_8   = 0,
    mode_8_16  = 1,
    mode_16_8  = 2,
    mode_16_16 = 3
  };
  enum class FormatMode {
    raw = 0,
    rgb = 1,
    yuv = 2,
    jpg = 3,
    mon = 4,
    raw_d = 5,
    mon_d = 6,
  };

  struct CameraConfig {
    uint32_t	camera_type;
    uint16_t  vendor_id;
    uint32_t	width;
    uint32_t	height;
    uint8_t	pixel_bytes;
    uint8_t   pixel_bits;
    uint32_t  i2c_address;
    uint32_t  size;
    uint8_t   usb_type; 
    I2cMode i2c_mode;
    FormatMode image_format_mode;
    uint32_t	trans_lvl;
    uint8_t color_mode;
  };

  ZoomCamera(std::string config_file, std::string i2c_device);
  ZoomCamera(std::string config_file, std::string i2c_device, uint32_t usb_index);
  ~ZoomCamera();

  static std::vector<CameraInfo> scan();

  bool getFrame(cv::Mat &frame);

  void setExposure(uint16_t exposure);
  void setGainGlobalAnalog(uint16_t gain);
  void setGainGlobalDigital(uint16_t gain);
  void setGainWhitebalance(uint16_t gain_gr, uint16_t gain_r, uint16_t gain_b, uint16_t gain_gb);

  void setZoom(uint16_t zoom);
  void setFocus(uint16_t focus);
  void setIrCut(bool ir_cut);

private:
  static void runImageCapture(ZoomCamera &zoom_camera);

  void *handle;
  CameraConfig camera_config;
  CameraInfo camera_info;

  uint8_t serial_number[16];

  std::thread capture_thread;
  bool running = true;

  int i2c_file_descriptor;
};

}
