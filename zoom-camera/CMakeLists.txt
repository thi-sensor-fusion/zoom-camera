cmake_minimum_required(VERSION 3.18.0)

find_package(OpenCV REQUIRED)
#find_package(CUDAToolkit 10.0 REQUIRED)

# Set the target name from the project name.
set(TARGET_NAME_EXECUTABLE ${LOCAL_PROJECT_NAME})
set(TARGET_NAME_LIBRARY ${LOCAL_PROJECT_FULL_NAME})

# Get the install path.
file(RELATIVE_PATH TARGET_RELATIVE_PATH ${PROJECT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR})
set(TARGET_PATH ${LOCAL_PROJECT_ROOT_NAME}/${TARGET_RELATIVE_PATH})

set(TARGET_SOURCES_EXECUTABLE
  main.cpp
)

set(TARGET_SOURCES_LIBRARY
  zoom_camera.cpp
  utils.cpp
)

set(TARGET_HEADERS_LIBRARY
  zoom_camera.hpp
)


# Create executable target for the project.
add_executable(${TARGET_NAME_EXECUTABLE} ${TARGET_SOURCES_EXECUTABLE})

# Create library target for the project.
add_library(${TARGET_NAME_LIBRARY} ${TARGET_HEADERS_LIBRARY} ${TARGET_SOURCES_LIBRARY})


# Add include directories to the library target.
target_include_directories(${TARGET_NAME_LIBRARY} PRIVATE ${OpenCV_INCLUDE_DIRS})

# Link external libraries to the library target.
target_link_libraries(${TARGET_NAME_LIBRARY} PRIVATE pthread ${OpenCV_LIBS} ArduCamLib usb-1.0 arducam_config_parser i2c)


# Add include directories to the executable.
target_include_directories(${TARGET_NAME_EXECUTABLE} PRIVATE ${OpenCV_INCLUDE_DIRS})

# Link the library to the executable.
target_link_libraries(${TARGET_NAME_EXECUTABLE} PRIVATE ${TARGET_NAME_LIBRARY} ${OpenCV_LIBS} pthread ArduCamLib usb-1.0 arducam_config_parser)


# Add public header install target.
install(FILES ${TARGET_HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${TARGET_PATH})
