#pragma once

#include <thi-sensor-fusion/zoom-camera/ArduCamLib.h>
#include <opencv2/opencv.hpp>

#include <thi-sensor-fusion/zoom-camera/zoom_camera.hpp>

namespace thi_sensor_fusion {
  cv::Mat convertImage(ArduCamOutData* frame_data, ZoomCamera::CameraConfig &camera_config);
}
