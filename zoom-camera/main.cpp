#include <iostream>
#include <vector>
#include <chrono>
#include <cstdint>
#include <regex>
#include <deque>
#include <memory>
#include <thread>

#include <opencv2/opencv.hpp>
#include <opencv2/cudacodec.hpp>
#include <opencv2/videoio.hpp>

#include "arducam_config_parser.h"
#include <thi-sensor-fusion/zoom-camera/ArduCamLib.h>

#include <thi-sensor-fusion/zoom-camera/zoom_camera.hpp>


const std::string generateEncodePipeline(std::string host, uint16_t port, uint16_t width, uint16_t height) {
  return "appsrc ! videoconvert ! video/x-raw, width=(int)" + std::to_string(width) + ", height=(int)" + std::to_string(height) + " ! nvvidconv ! nvv4l2h264enc idrinterval=1 control-rate=0 peak-bitrate=40000000 ! rtph264pay ! udpsink host=" + host + " port=" + std::to_string(port);
}

void printUsage() {
  std::cout << "Usage: zoom-camera <path to camera.cfg> <path to i2c device>" << std::endl;
  exit(1);
}

int main(int argc, char **argv) {
  if (argc < 3) {
    printUsage();
  }

  const uint16_t framerate = 30;

  const uint16_t output_resolution_x = 1920;
  const uint16_t output_resolution_y = 1080;

  const std::string encode_pipeline = generateEncodePipeline("10.0.0.36", 1234, output_resolution_x, output_resolution_y);
  std::cout << encode_pipeline << std::endl;

  cv::VideoWriter writer = cv::VideoWriter(encode_pipeline, cv::CAP_GSTREAMER, 0, (double)framerate, cv::Size(output_resolution_x, output_resolution_y), true);
  cv::Mat output;

  thi_sensor_fusion::ZoomCamera zoom_camera(argv[1], argv[2]);

  uint i = 0;
  uint n = 0;

  while (true) {
    if (zoom_camera.getFrame(output)) {

      writer << output;

      zoom_camera.setFocus(i);
      zoom_camera.setZoom(n + 3000);

      i = (i + 30) % 19000;
      i = (i + 25) % (19000 - 3000);

    }
  }
}