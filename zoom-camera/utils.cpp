#include <thi-sensor-fusion/zoom-camera/utils.hpp>

namespace thi_sensor_fusion {

    
cv::Mat JPGToMat(Uint8* bytes, int length) {
	cv::Mat image = cv::Mat(1, length, CV_8UC1, bytes);
	if (length <= 0) {
		image.data = NULL;
		return image;
	}

	image = imdecode(image, cv::IMREAD_ANYCOLOR);
	return image;
}
cv::Mat YUV422toMat(Uint8* bytes, int width, int height)
{
	cv::Mat image = cv::Mat(height, width, CV_8UC2, bytes);
	cv::cvtColor(image, image, cv::COLOR_YUV2BGR_YUYV);
	return image;
}

cv::Mat separationImage(Uint8* bytes, int width, int height)
{
	int width_d = width << 1;
	unsigned char* temp1, *temp2;
	temp1 = (unsigned char*)malloc(width);
	temp2 = (unsigned char*)malloc(width);

	for (int k = 0; k < height; k++) {
		for (int i = 0, j = 0; i < width_d; i += 2) {
			temp1[j] = bytes[i + (k * width_d)];
			temp2[j++] = bytes[i + 1 + (k * width_d)];
		}
		memcpy(bytes + (k * width_d), temp1, width);
		memcpy(bytes + (k * width_d + width), temp2, width);
	}
	cv::Mat image = cv::Mat(height, width_d, CV_8UC1, bytes);
	free(temp1);
	free(temp2);
	return image;
}

#define RGB565_RED      0xf800
#define RGB565_GREEN    0x07e0
#define RGB565_BLUE     0x001f

cv::Mat RGB565toMat(Uint8* bytes, int width, int height, uint8_t color_mode) {
	unsigned char* temp_data, *ptdata, *data, *data_end;

	data = bytes;
	data_end = bytes + (width * height * 2);

	temp_data = (unsigned char*)malloc(width * height * 3);
	ptdata = temp_data;

	Uint8 r, g, b;
	while (data < data_end) {
		unsigned short temp;

		temp = (*data << 8) | *(data + 1);
		r = (temp & RGB565_RED) >> 8;
		g = (temp & RGB565_GREEN) >> 3;
		b = (temp & RGB565_BLUE) << 3;

		switch (color_mode) {
		case 1:
			*ptdata++ = r;
			*ptdata++ = g;
			*ptdata++ = b;
			break;
		case 0:
		default:
			*ptdata++ = b;
			*ptdata++ = g;
			*ptdata++ = r;
			break;
		}
		data += 2;
	}

	cv::Mat image = cv::Mat(height, width, CV_8UC3);
	memcpy(image.data, temp_data, height * width * 3);
	cv::flip(image, image, 0);
	free(temp_data);
	return image;
}


cv::Mat dBytesToMat(Uint8* bytes, int bit_width, int width, int height) {
	unsigned char* temp_data = (unsigned char*)malloc(width * height);
	int index = 0;
	for (int i = 0; i < width * height * 2; i += 2) {
		unsigned char temp = ((bytes[i + 1] << 8 | bytes[i]) >> (bit_width - 8)) & 0xFF;
		temp_data[index++] = temp;
	}
	cv::Mat image = cv::Mat(height, width, CV_8UC1);
	memcpy(image.data, temp_data, height * width);
	free(temp_data);
	return image;
}

cv::Mat BytestoMat(Uint8* bytes, int width, int height)
{
	cv::Mat image = cv::Mat(height, width, CV_8UC1, bytes);
	return image;
}

cv::Mat convertImage(ArduCamOutData* frameData, ZoomCamera::CameraConfig &camera_config) {
	cv::Mat rawImage;
	Uint8* data = frameData->pu8ImageData;
	int height, width;
	width = camera_config.width;
	height = camera_config.height;

	switch (camera_config.image_format_mode) {
	case ZoomCamera::FormatMode::rgb:
		rawImage = RGB565toMat(data, width, height, camera_config.color_mode);
		break;
	case ZoomCamera::FormatMode::raw_d:
		rawImage = separationImage(data, width, height);
		switch (camera_config.color_mode) {
		case RAW_RG:cv::cvtColor(rawImage, rawImage, cv::COLOR_BayerRG2BGR);
			break;
		case RAW_GR:cv::cvtColor(rawImage, rawImage, cv::COLOR_BayerGR2BGR);
			break;
		case RAW_GB:cv::cvtColor(rawImage, rawImage, cv::COLOR_BayerGB2BGR);
			break;
		case RAW_BG:cv::cvtColor(rawImage, rawImage, cv::COLOR_BayerBG2BGR);
			break;
		default:
			cv::cvtColor(rawImage, rawImage, cv::COLOR_BayerRG2BGR);
			break;
		}
		break;
	case ZoomCamera::FormatMode::mon_d:
		rawImage = separationImage(data, width, height);
		break;
	case ZoomCamera::FormatMode::jpg:
		rawImage = JPGToMat(data, frameData->stImagePara.u32Size);
		break;
	case ZoomCamera::FormatMode::raw:
		if (camera_config.pixel_bytes == 2) {
			rawImage = dBytesToMat(data, frameData->stImagePara.u8PixelBits, width, height);
		}
		else {
			rawImage = BytestoMat(data, width, height);
		}
		switch (camera_config.color_mode) {
		case RAW_RG:cv::cvtColor(rawImage, rawImage, cv::COLOR_BayerRG2BGR);
			break;
		case RAW_GR:cv::cvtColor(rawImage, rawImage, cv::COLOR_BayerGR2BGR);
			break;
		case RAW_GB:cv::cvtColor(rawImage, rawImage, cv::COLOR_BayerGB2BGR);
			break;
		case RAW_BG:cv::cvtColor(rawImage, rawImage, cv::COLOR_BayerBG2BGR);
			break;
		default:
			cv::cvtColor(rawImage, rawImage, cv::COLOR_BayerRG2BGR);
			break;
		}
		break;
	case ZoomCamera::FormatMode::yuv:
		rawImage = YUV422toMat(data, width, height);
		break;
	case ZoomCamera::FormatMode::mon:
		if (camera_config.pixel_bytes == 2) {
			rawImage = dBytesToMat(data, frameData->stImagePara.u8PixelBits, width, height);
		}
		else {
			rawImage = BytestoMat(data, width, height);
		}
		break;
	default:
		if (camera_config.pixel_bytes == 2) {
			rawImage = dBytesToMat(data, frameData->stImagePara.u8PixelBits, width, height);
		}
		else {
			rawImage = BytestoMat(data, width, height);
		}
		cv::cvtColor(rawImage, rawImage, cv::COLOR_BayerRG2RGB);
		break;
	}

	return rawImage;
}
  
}
