#include <thi-sensor-fusion/zoom-camera/zoom_camera.hpp>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "linux/i2c-dev.h"
#include "linux/i2c.h"
#include <i2c/smbus.h>

#include <thi-sensor-fusion/zoom-camera/ArduCamLib.h>
#include "arducam_config_parser.h"

#include <thi-sensor-fusion/zoom-camera/utils.hpp>


namespace thi_sensor_fusion {

  static inline bool openConfigFile(std::string config_file, ZoomCamera::CameraConfig &camera_config, CameraConfigs &extra_configurations) {
    memset(&extra_configurations, 0x00, sizeof(CameraConfigs));
    if (arducam_parse_config(config_file.c_str(), &extra_configurations)) {
      return false;
    }

    CameraParam *camera_parameter = &extra_configurations.camera_param;

    switch (camera_parameter->i2c_mode) {
    case 0: camera_config.i2c_mode = ZoomCamera::I2cMode::mode_8_8; break;
    case 1: camera_config.i2c_mode = ZoomCamera::I2cMode::mode_8_16; break;
    case 2: camera_config.i2c_mode = ZoomCamera::I2cMode::mode_16_8; break;
    case 3: camera_config.i2c_mode = ZoomCamera::I2cMode::mode_16_16; break;
    default:      break;
    }

    switch (camera_parameter->format >> 8) {
    case 0: camera_config.image_format_mode = ZoomCamera::FormatMode::raw; break;
    case 1: camera_config.image_format_mode = ZoomCamera::FormatMode::rgb; break;
    case 2: camera_config.image_format_mode = ZoomCamera::FormatMode::yuv; break;
    case 3: camera_config.image_format_mode = ZoomCamera::FormatMode::jpg; break;
    case 4: camera_config.image_format_mode = ZoomCamera::FormatMode::mon; break;
    case 5: camera_config.image_format_mode = ZoomCamera::FormatMode::raw_d; break;
    case 6: camera_config.image_format_mode = ZoomCamera::FormatMode::mon_d; break;
    default: break;
    }

    camera_config.width = camera_parameter->width;
    camera_config.height = camera_parameter->height;

    camera_config.i2c_address = camera_parameter->i2c_addr;
    camera_config.pixel_bits = camera_parameter->bit_width;
    camera_config.trans_lvl = camera_parameter->trans_lvl;

    if (camera_config.pixel_bits <= 8) {
      camera_config.pixel_bytes = 1;
    } else if (camera_config.pixel_bits > 8 && camera_config.pixel_bits <= 16) {
      camera_config.pixel_bytes = 2;
    }

    camera_config.color_mode = camera_parameter->format & 0xFF;

    return true;
  }

  static inline void cameraConfigToArducamConfig(ZoomCamera::CameraConfig &camera_config, ArduCamCfg &arducam_config) {
    arducam_config.u32CameraType = camera_config.camera_type;
    arducam_config.u16Vid = camera_config.vendor_id;
    arducam_config.u32Width = camera_config.width;
    arducam_config.u32Height = camera_config.height;
    arducam_config.u8PixelBytes = camera_config.pixel_bytes;
    arducam_config.u8PixelBits = camera_config.pixel_bits;
    arducam_config.u32I2cAddr = camera_config.i2c_address;
    arducam_config.u32Size = camera_config.size;
    arducam_config.usbType = camera_config.usb_type; 
    arducam_config.emI2cMode = (i2c_mode)camera_config.i2c_mode;
    arducam_config.emImageFmtMode = (format_mode)camera_config.image_format_mode;
    arducam_config.u32TransLvl = camera_config.trans_lvl;
  }

  static inline void applyExtraConfigurations(ArduCamHandle handle, ZoomCamera::CameraConfig &camera_config, CameraConfigs &extra_configurations, ZoomCamera::CameraInfo &camera_info) {
    Config *configs = extra_configurations.configs;
    uint32_t configs_length = extra_configurations.configs_length;

		for (uint32_t i = 0; i < configs_length; i++) {
			uint32_t type = configs[i].type;
			if (((type >> 16) & 0xFF) && ((type >> 16) & 0xFF) != camera_config.usb_type) {
				continue;
      }

			switch (type & 0xFFFF) {
			case CONFIG_TYPE_REG:
				ArduCam_writeSensorReg(handle, configs[i].params[0], configs[i].params[1]);
				break;
			case CONFIG_TYPE_DELAY:
				usleep(1000 * configs[i].params[0]);
				break;
			case CONFIG_TYPE_VRCMD:
				uint8_t u8Buf[10];
        for (uint32_t n = 0; n < configs[i].params[3]; n++) {
          u8Buf[n] = configs[i].params[4 + n];
        }
        ArduCam_setboardConfig(handle, configs[i].params[0], configs[i].params[1], configs[i].params[2], configs[i].params[3], u8Buf);
				break;
			}
		}
		ArduCam_registerCtrls(handle, extra_configurations.controls, extra_configurations.controls_length);
		ArduCam_readUserData(handle, 0x400 - 16, 16, camera_info.serial_number.data());
  }

  ZoomCamera::ZoomCamera(std::string config_file, std::string i2c_device) {

    CameraConfigs extra_configurations;
    openConfigFile(config_file, camera_config, extra_configurations);

    ArduCamCfg arducam_config;
    cameraConfigToArducamConfig(camera_config, arducam_config);

    ArduCam_autoopen(handle, &arducam_config);

    applyExtraConfigurations(handle, camera_config, extra_configurations, camera_info);

    
	  ArduCam_setCtrl(handle, "setFramerate", 30);
    
		ArduCam_setMode(handle, CONTINUOUS_MODE);
    
    capture_thread = std::thread(ZoomCamera::runImageCapture, std::ref(*this));

    i2c_file_descriptor = open(i2c_device.c_str(), O_RDWR);
    if (i2c_file_descriptor < 0) {
      perror("Unable to open device node.");
      exit(1);
    }

    int ret_val = ioctl(i2c_file_descriptor, I2C_SLAVE_FORCE, I2C_CHIP_ADDRESS);
    if (ret_val < 0) {
      perror("Could not set I2C_SLAVE.");
      exit(2);
    }
  }

  ZoomCamera::ZoomCamera(std::string config_file, std::string i2c_device, uint32_t usb_index) {

    CameraConfigs extra_configurations;
    openConfigFile(config_file, camera_config, extra_configurations);

    ArduCamCfg arducam_config;
    cameraConfigToArducamConfig(camera_config, arducam_config);

    ArduCam_open(handle, &arducam_config, usb_index);

    applyExtraConfigurations(handle, camera_config, extra_configurations, camera_info);

    
	  ArduCam_setCtrl(handle, "setFramerate", 30);
    
		ArduCam_setMode(handle, CONTINUOUS_MODE);

    capture_thread = std::thread(ZoomCamera::runImageCapture, std::ref(*this));
    
    i2c_file_descriptor = open(i2c_device.c_str(), O_RDWR);
    if (i2c_file_descriptor < 0) {
      perror("Unable to open device node.");
      exit(1);
    }

    int ret_val = ioctl(i2c_file_descriptor, I2C_SLAVE_FORCE, I2C_CHIP_ADDRESS);
    if (ret_val < 0) {
      perror("Could not set I2C_SLAVE.");
      exit(2);
    }
  }

  ZoomCamera::~ZoomCamera() {
    running = false;
    capture_thread.join();
    close(i2c_file_descriptor);
		ArduCam_close(handle);
  }

  std::vector<ZoomCamera::CameraInfo> ZoomCamera::scan() {

    ArduCamIndexinfo camera_info_array[16];
    int camera_count = 0;
    camera_count = ArduCam_scan(camera_info_array);

    std::vector<CameraInfo> camera_infos(camera_count);
    for (ssize_t i = 0; i < camera_count; i++) {
      camera_infos[i].usb_index = camera_info_array[i].u8UsbIndex;
      camera_infos[i].serial_number = std::to_array(camera_info_array[i].u8SerialNum);
    }
    
    return camera_infos;
  }


  bool ZoomCamera::getFrame(cv::Mat &frame) {

    ArduCamOutData* frameData;

    if (ArduCam_availableImage(handle) > 0) {
			Uint32 rtn_val = ArduCam_readImage(handle, frameData);

			if (rtn_val == USB_CAMERA_NO_ERROR) {

				frame = convertImage(frameData, camera_config);
				ArduCam_del(handle);

        if (!frame.data) {
          return false;
        }
				
				return true;
			}
		}
    return false;
  }
  

  void ZoomCamera::setExposure(uint16_t exposure) {
    ArduCam_writeSensorReg(handle, 0x0202, (exposure & 0xFF00) >> 8);
    ArduCam_writeSensorReg(handle, 0x0203, (exposure & 0x00FF) >> 0);
  }


  void ZoomCamera::setGainGlobalAnalog(uint16_t gain) {
    ArduCam_writeSensorReg(handle, 0x0204, (gain & 0xFF00) >> 8);
    ArduCam_writeSensorReg(handle, 0x0205, (gain & 0x00FF) >> 0);
  }


  void ZoomCamera::setGainGlobalDigital(uint16_t gain) {
    ArduCam_writeSensorReg(handle, 0x3FF9, 0x01);

    ArduCam_writeSensorReg(handle, 0x020E, (gain & 0xFF00) >> 8);
    ArduCam_writeSensorReg(handle, 0x020F, (gain & 0x00FF) >> 0);
  }


  void ZoomCamera::setGainWhitebalance(uint16_t gain_gr, uint16_t gain_r, uint16_t gain_b, uint16_t gain_gb) {
    ArduCam_writeSensorReg(handle, 0x3FF9, 0x00);

    ArduCam_writeSensorReg(handle, 0x020E, (gain_gr & 0xFF00) >> 8);
    ArduCam_writeSensorReg(handle, 0x020F, (gain_gr & 0x00FF) >> 0);
    ArduCam_writeSensorReg(handle, 0x0210, (gain_r & 0xFF00) >> 8);
    ArduCam_writeSensorReg(handle, 0x0211, (gain_r & 0x00FF) >> 0);
    ArduCam_writeSensorReg(handle, 0x0212, (gain_b & 0xFF00) >> 8);
    ArduCam_writeSensorReg(handle, 0x0213, (gain_b & 0x00FF) >> 0);
    ArduCam_writeSensorReg(handle, 0x0214, (gain_gb & 0xFF00) >> 8);
    ArduCam_writeSensorReg(handle, 0x0215, (gain_gb & 0x00FF) >> 0);
  }

  int writeI2c(int file_descriptor, uint8_t address, uint16_t value) {
    i2c_smbus_data data;
    data.word = ((value & 0x00FF)<< 8) | ((value & 0xFF00) >> 8);
    i2c_smbus_ioctl_data payload = {
        .read_write = I2C_SMBUS_WRITE,
        .command = address,
        .size = I2C_SMBUS_WORD_DATA,
        .data = &(data),
    };
    int result = ioctl(file_descriptor, I2C_SMBUS, &payload);
    if (result < 0) {
        result = -errno;
    }
    return result;
  }

  void ZoomCamera::setZoom(uint16_t zoom) {
    int result = 0;
    if (zoom != 0xffff) {
      result = writeI2c(i2c_file_descriptor, I2C_ZOOM_REGISTER_ADDRESS, zoom);
    } else {
      result = writeI2c(i2c_file_descriptor, I2C_ZOOM_RESET_ADDRESS, 3000);
      result = writeI2c(i2c_file_descriptor, I2C_ZOOM_REGISTER_ADDRESS, 0);
    }

    if (result < 0) {
      perror("I2C Write Operation failed.");
      exit(4);
    }
  }


  void ZoomCamera::setFocus(uint16_t focus) {
    int result = 0;
    if (focus != 0xffff) {
      result = writeI2c(i2c_file_descriptor, I2C_FOCUS_REGISTER_ADDRESS, focus);
    } else {
      result = writeI2c(i2c_file_descriptor, I2C_FOCUS_RESET_ADDRESS, 0);
      result = writeI2c(i2c_file_descriptor, I2C_FOCUS_REGISTER_ADDRESS, 0);
    }

    if (result < 0) {
      perror("I2C Write Operation failed.");
      exit(4);
    }
  }


  void ZoomCamera::setIrCut(bool ir_cut) {
    int result = 0;
    if (ir_cut == true) {
      result = writeI2c(i2c_file_descriptor, I2C_IRCUT_REGISTER_ADDRESS, 0x0000);
    } else {
      result = writeI2c(i2c_file_descriptor, I2C_IRCUT_REGISTER_ADDRESS, 0x0001);
    }

    if (result < 0) {
      perror("I2C Write Operation failed.");
      exit(4);
    }
  }


  

  void ZoomCamera::runImageCapture(ZoomCamera &zoom_camera) {
    uint32_t result = ArduCam_beginCaptureImage(zoom_camera.handle);
    if (result == USB_CAMERA_USB_TASK_ERROR) {
      std::cout << "Error beginning capture, rtn_val = " << result << std::endl;
      return;
    }
    else {
      std::cout << "Capture began, rtn_val = " << result << std::endl;
    }

    while (zoom_camera.running) {
      result = ArduCam_captureImage(zoom_camera.handle);
      if (result == USB_CAMERA_USB_TASK_ERROR) {
        std::cout << "Error capture image, rtn_val = " << result << std::endl;
        break;
      }
      else if (result > 0xFF) {
        std::cout << "Error capture image, rtn_val = " << result << std::endl;
      }
    }

    ArduCam_endCaptureImage(zoom_camera.handle);
    std::cout << "Capture thread stopped." << std::endl;
  }

}
